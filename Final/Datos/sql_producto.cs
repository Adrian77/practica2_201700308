﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;



namespace Final.Datos
{
    public class sql_producto
    {
        public void insertarProducto(String codigo_producto, String descripcion, String cantidad_disponible, String costo_unidad, String codigo_usuario, String codigo_tipo, String codigo_material, String codigo_color)
        {

            MySqlConnection conectar = Conexion.RecibirConexion();

            try
            {

                conectar.Open();

                MySqlCommand comando = new MySqlCommand();

                comando.Connection = conectar;

                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = "INSERT INTO producto VALUES(@a,@b,@c,@d,@e,@f,@g,@h);";

                //comando.CommandText = "INSERT INTO producto VALUES(001,Computadora DELL, 50,GTQ 500.00, 01,03,0.2,0.1)";

                comando.Parameters.AddWithValue("@a", codigo_producto);
                comando.Parameters.AddWithValue("@b", descripcion);
                comando.Parameters.AddWithValue("@c", cantidad_disponible);
                comando.Parameters.AddWithValue("@d", costo_unidad);
                comando.Parameters.AddWithValue("@e", codigo_usuario);
                comando.Parameters.AddWithValue("@f", codigo_tipo);
                comando.Parameters.AddWithValue("@g", codigo_material);
                comando.Parameters.AddWithValue("@h", codigo_color);

                try
                {
                    comando.ExecuteNonQuery();
                    conectar.Close();

                }
                catch (Exception e)
                {

                    Console.WriteLine(e.ToString());

                }


            }
            catch (Exception s)
            {
                Console.WriteLine(s.ToString());
            }


        }

    }
}