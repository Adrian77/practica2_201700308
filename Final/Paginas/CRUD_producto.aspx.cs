﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Final.Datos;


namespace Final.Paginas
{
    public partial class CRUD_producto : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            String codigo_producto = Convert.ToString(TextBox1.Text);
            String descripcion = Convert.ToString(TextBox2.Text);
            String cantidad_disponible = Convert.ToString(TextBox3.Text);
            String costo_unidad = Convert.ToString(TextBox4.Text);
            String codigo_usuario = Convert.ToString(TextBox5.Text);
            String codigo_tipo = Convert.ToString(TextBox6.Text);
            String codigo_material = Convert.ToString(TextBox7.Text);
            String codigo_color = Convert.ToString(TextBox8.Text);


            sql_producto insercion = new sql_producto();

            insercion.insertarProducto(codigo_producto, descripcion, cantidad_disponible, costo_unidad, codigo_usuario, codigo_tipo, codigo_material, codigo_color);


        }
    }
}