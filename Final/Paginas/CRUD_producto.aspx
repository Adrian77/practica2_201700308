﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CRUD_producto.aspx.cs" Inherits="Final.Paginas.CRUD_producto" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">

        .auto-style1 {
            width: 131px;
        }
        .auto-style2 {
            width: 136px;
        }
        .auto-style3 {
            width: 133px;
        }
        .auto-style6 {
            width: 169px;
        }
        .auto-style4 {
            width: 133px;
            height: 26px;
        }
        .auto-style7 {
            height: 26px;
            width: 169px;
        }
        .auto-style5 {
            height: 26px;
        }
        </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div>
                <br />
                CRUD productos<br />
                <br />
                <asp:Button ID="Button2" runat="server" Text="Refrescar pagina" />
                <br />
                <br />
                Crear producto<br />
                <br />
                <br />
                <br />
                <table style="width: 100%;">
                    <tr>
                        <td class="auto-style1">Codigo producto</td>
                        <td class="auto-style2">
                            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="auto-style1">Descripcion</td>
                        <td class="auto-style2">
                            <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="auto-style1">Cantidad disponible</td>
                        <td class="auto-style2">
                            <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="auto-style1">Costo unidad</td>
                        <td class="auto-style2">
                            <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="auto-style1">Codigo usuario</td>
                        <td class="auto-style2">
                            <asp:TextBox ID="TextBox5" runat="server"></asp:TextBox>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="auto-style1">Codigo tipo</td>
                        <td class="auto-style2">
                            <asp:TextBox ID="TextBox6" runat="server"></asp:TextBox>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="auto-style1">Codigo material</td>
                        <td class="auto-style2">
                            <asp:TextBox ID="TextBox7" runat="server"></asp:TextBox>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="auto-style1">Codigo color</td>
                        <td class="auto-style2">
                            <asp:TextBox ID="TextBox8" runat="server"></asp:TextBox>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
                <br />
                <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="crear producto" />
                <br />
                <br />
                Editar producto<br />
                <asp:DropDownList ID="DropDownList1" runat="server" Width="270px">
                </asp:DropDownList>
                <br />
                <br />
                <table style="width:100%;">
                    <tr>
                        <td class="auto-style3">Codigo producto</td>
                        <td class="auto-style6">
                            <asp:TextBox ID="TextBox9" runat="server"></asp:TextBox>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="auto-style3">Descripcion</td>
                        <td class="auto-style6">
                            <asp:TextBox ID="TextBox10" runat="server"></asp:TextBox>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="auto-style3">Cantidad disponible</td>
                        <td class="auto-style6">
                            <asp:TextBox ID="TextBox11" runat="server"></asp:TextBox>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="auto-style3">Costo unidad</td>
                        <td class="auto-style6">
                            <asp:TextBox ID="TextBox12" runat="server"></asp:TextBox>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="auto-style3">Codigo usuario</td>
                        <td class="auto-style6">
                            <asp:TextBox ID="TextBox13" runat="server"></asp:TextBox>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="auto-style4">Codigo tipo</td>
                        <td class="auto-style7">
                            <asp:TextBox ID="TextBox14" runat="server"></asp:TextBox>
                        </td>
                        <td class="auto-style5"></td>
                    </tr>
                    <tr>
                        <td class="auto-style3">Codigo material</td>
                        <td class="auto-style6">
                            <asp:TextBox ID="TextBox15" runat="server"></asp:TextBox>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="auto-style3">Codigo color</td>
                        <td class="auto-style6">
                            <asp:TextBox ID="TextBox16" runat="server"></asp:TextBox>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
                <br />
                <br />
                <br />
                Visualizar productos<br />
                <br />
                <asp:GridView ID="GridView1" runat="server">
                </asp:GridView>
                <br />
                <br />
                <br />
                <br />
                <br />
            </div>
        </div>
    </form>
</body>
</html>
